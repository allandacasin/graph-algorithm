import React, { useState } from 'react';
import graph from './utils/graph';
import Alert from './components/Alert';
import AppDescription from './components/AppDescription';

const initAlert = {
  message: '',
  compClass: '',
};

const App: React.FC = () => {
  const [paths, setPaths] = useState('');
  const [alert, setAlert] = useState(initAlert);

  const clearPaths = () => {
    //
    setPaths('');
    setAlert(initAlert);
  };

  const handleOnChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    //
    setPaths(e.target.value);
  };

  const handleOnClick = () => {
    //
    if (!paths) {
      setAlert({
        message: 'Please enter paths.',
        compClass: 'bg-yellow-300',
      });
      return;
    }
    const isConnected = graph.isConnected(paths);
    const isBipartite = graph.isBipartite();

    switch (true) {
      case !isConnected:
        setAlert({
          message: 'Is not a connected graph.',
          compClass: 'bg-red-300',
        });
        break;
      case isConnected && isBipartite:
        setAlert({
          message: 'Is a connected and red-blue colorable graph.',
          compClass: 'bg-green-700 text-white ',
        });
        break;
      case isConnected && !isBipartite:
        setAlert({
          message: 'Is a connected graph, but not red-blue colorable.',
          compClass: 'bg-yellow-300',
        });
        break;
      default:
        break;
    }
  };

  return (
    <div className="flex flex-row justify-center px-5 items-center h-screen">
      <div className="w-full sm:w-9/12 lg:w-6/12 xl:w-5/12">
        <AppDescription />
        <Alert message={alert.message} compClass={alert.compClass} />
        <form>
          <div className="mb-3">
            <label htmlFor="paths">
              Paths
              <textarea
                onChange={handleOnChange}
                value={paths}
                id="paths"
                placeholder="Enter paths ..."
                className="border border-gray-300 w-full"
              />
            </label>
          </div>

          <div className="flow-root">
            <div className="float-right">
              <button
                onClick={handleOnClick}
                type="button"
                className="bg-blue-500 hover:bg-blue-400 py-2 px-5 mr-5"
              >
                Check
              </button>

              <button
                onClick={clearPaths}
                type="button"
                className="bg-gray-300 hover:bg-gray-200 py-2 px-5"
              >
                Clear
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default App;
