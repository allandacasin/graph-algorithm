import React from 'react';

const AppDescription: React.FC = (props) => (
  <>
    <h1 className="text-3xl mb-3">Graph Algorithm</h1>
    <p className="mb-3 text-justify">
      Application to check if a graph is red-blue colorable. A graph is red-blue
      colorable if two connected nodes have never the same color and the graph
      is a connected graph. A user should be able to enter a graph in a textarea
      by typing some paths (a word is a node, a dash an edge and a new line or a
      comma a separation between paths).
    </p>
    <h2>Some examples: </h2>
    <ul className="mb-3">
      <li className="mb-2">
        Input: a - b - c <br /> Is a connected and red-blue colorable graph.
      </li>
      <li className="mb-2">
        Input: a - b, f - g <br /> Is not a connected graph.
      </li>
      <li className="mb-2">
        Input: a - b - c - a <br /> Is a connected graph, but not red blue
        colorable.
      </li>
      <li className="mb-2">
        Input: a - b, c - d, b - c, a - d <br />
        Is a connected and red-blue colorable graph
      </li>
    </ul>
  </>
);

export default AppDescription;
