import React from 'react';
import PropTypes from 'prop-types';

interface Props {
  message: string | null;
  compClass: string | null;
}

const Alert: React.FC<Props> = ({ message, compClass }) => (
  <>
    {message ? (
      <div className={`p-1 ${compClass || ''}`}>
        <p>{message}</p>
      </div>
    ) : (
      ''
    )}
  </>
);

Alert.propTypes = {
  message: PropTypes.string,
  compClass: PropTypes.string,
};

export default Alert;
