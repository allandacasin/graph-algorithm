interface AdjacencyList {
  [key: string]: string[];
}

interface Visited {
  [key: string]: string | boolean;
}

let adjacencyList: AdjacencyList = {};

const addNode = (node: string): void => {
  //
  if (!adjacencyList[node]) {
    adjacencyList[node] = [];
  }
};

const addEdge = (n1: string, n2: string): void => {
  //
  adjacencyList[n1].push(n2);
  adjacencyList[n2].push(n1);
};

const depthFirstRecursive = (start: string, visited: Visited): string[] => {
  //
  const result: string[] = [];

  const dfs = (node: string) => {
    //
    if (!node) {
      return null;
    }
    visited[node] = true;
    result.push(node);
    adjacencyList[node].forEach((neighbor: string) => {
      if (!visited[neighbor]) {
        dfs(neighbor);
      }
    });
  };

  dfs(start);

  return result;
};

const setNodes = (paths: string): void => {
  //
  const nodes = paths.split(/[\s,-]+/);
  for (let n = 0; n < nodes.length; n++) {
    addNode(nodes[n]);
  }
};

const setEdges = (paths: string): void => {
  //
  const edges: any = [];

  // split paths by comma and line break
  const tempEdges = paths.split(/\s*[,\n]+\s*/);

  for (let t = 0; t < tempEdges.length; t++) {
    edges.push(tempEdges[t].split('-'));
  }
  // O(a*b)
  for (let e = 0; e < edges.length; e++) {
    for (let i = 0; i < edges[e].length - 1; i++) {
      addEdge(edges[e][i], edges[e][i + 1]);
    }
  }
};

const isConnected = (paths: string): boolean => {
  // re-init
  adjacencyList = {};

  // trim empty line breaks and spaces
  const p = paths
    .trim()
    .replace(/^\s*[\r\n]/gm, '')
    .replace(/ /g, '');

  setNodes(p);

  setEdges(p);

  const visited: Visited = {};

  const result: any = [];

  for (const node of Object.keys(adjacencyList)) {
    //
    visited[node] = false;
  }

  for (const node of Object.keys(adjacencyList)) {
    //
    if (!visited[node]) {
      result.push(depthFirstRecursive(node, visited));
    }
  }

  // graph is not connected if connected components is greater than 1
  if (result.length > 1) {
    return false;
  }

  return true;
};

const isBipartite = (): boolean => {
  // set starting node
  let start = '';
  for (const node of Object.keys(adjacencyList)) {
    start = node;
    break;
  }
  // Breadth-first
  // init variables
  const queue = [start];
  let result = true;
  //
  const firstColor = 'red';
  const secondColor = 'blue';
  //
  const visited: Visited = {};
  let currentNode: any = start;
  visited[start] = firstColor;

  while (queue.length) {
    currentNode = queue.shift();

    let color = firstColor;
    const currentColor = visited[currentNode];

    if (currentColor === firstColor) {
      color = secondColor;
    } else {
      color = firstColor;
    }
    //
    let tempResult: boolean = result;

    adjacencyList[currentNode].forEach((neighbor: string) => {
      if (!visited[neighbor]) {
        visited[neighbor] = color;
        queue.push(neighbor);
      }
      if (currentColor === visited[neighbor]) {
        tempResult = false;
      }
    });

    result = tempResult;
  }
  return result;
};

export default {
  isConnected,
  isBipartite,
};
